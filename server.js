var express = require('express');

const PORT = 3000;

var app = express();
app.get('/', function(req, res) {
    res.send('Hola World');
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
